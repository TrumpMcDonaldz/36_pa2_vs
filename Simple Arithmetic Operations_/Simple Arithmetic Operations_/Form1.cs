﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Simple_Arithmetic_Operations_
{
    public partial class Form1 : Form
    {
        //Variables

        float FirstNum, SecNum, EndResult;

        public Form1()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void FirstNumField_TextChanged(object sender, EventArgs e)
        {

        }

        private void SecNumField_TextChanged(object sender, EventArgs e)
        {

        }


        private void AddButtton(object sender, EventArgs e)
        {
            Calc("Add");
        }

        private void SubtractButton(object sender, EventArgs e)
        {
            Calc("Sub");
        }

        private void MultiplyButton(object sender, EventArgs e)
        {

            Calc("Multi");
        }

        private void DivideButton(object sender, EventArgs e)
        {
            Calc("Div");
        }

        private void QuitButton(object sender, EventArgs e)
        {

        }

        private void Calc(string CalcType)
        {
            if (!float.TryParse(FirstNumField.Text, out FirstNum) || !float.TryParse(SecNumField.Text, out SecNum))
            {
                MessageBox.Show("Please enter a NUMERICAL value in above fields!");

                return;
            }

            switch (CalcType)
            {
                default:
                    {
                        MessageBox.Show("Internal Error.");

                        return;
                    }
                case "Add":
                    {
                        EndResult = FirstNum + SecNum;

                        break;
                    }
                case "Sub":
                    {
                        EndResult = FirstNum - SecNum;

                        break;
                    }
                case "Multi":
                    {
                        EndResult = FirstNum * SecNum;

                        break;
                    }
                case "Div":
                    {
                        if (SecNum == 0)
                        {
                            MessageBox.Show("Divide By Zero Error!");

                            return;
                        }

                        EndResult = FirstNum / SecNum;

                        break;
                    }
            }

            MessageBox.Show($"The end result of the two Numbers are {EndResult}");
        }
    }
}
